import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class RpnCalculatorTests {
    private lateinit var rpn: RpnCalculator

    @BeforeEach
    fun build() {
        rpn = RpnCalculator()
    }

    @Test
    fun `no calculation should give zero`() {
        assertEquals(0, rpn.calculate())
    }

    @Test
    fun `when inputs a number, should respond that number`() {
        rpn.input(41)
        assertEquals(41, rpn.calculate())
    }

    @Test
    fun `inputs two numbers with no operation, calculation should fail`() {
        rpn.input(65)
        rpn.input(24)
        assertThrows<RpnCalculator.CalculationNotFinished> { rpn.calculate() }
    }

    @Test
    fun `inputs one number and an operation, the calculation should fail`() {
        rpn.input(65)
        assertThrows<RpnCalculator.CalculationNotFinished> { rpn.multiplication() }
    }

    @Test
    fun `sums two values`() {
        rpn.input(2)
        rpn.input(3)
        rpn.addition()
        assertEquals(5, rpn.calculate())
    }

    @Test
    fun `subtracts two values`() {
        rpn.input(2)
        rpn.input(3)
        rpn.subtraction()
        assertEquals(-1, rpn.calculate())
    }

    @Test
    fun `adds and subtracts`() {
        rpn.input(4)
        rpn.input(2)
        rpn.addition()
        rpn.input(3)
        rpn.subtraction()
        assertEquals(3, rpn.calculate())
    }

    @Test
    fun `multiplies two numbers`() {
        rpn.input(3)
        rpn.input(4)
        rpn.multiplication()
        assertEquals(12, rpn.calculate())
    }

    @Test
    fun `divides two numbers`() {
        rpn.input(12)
        rpn.input(4)
        rpn.division()
        assertEquals(3, rpn.calculate())
    }

    @Test
    fun `calculates square roots`() {
        rpn.input(9)
        rpn.sqrt()
        assertEquals(3, rpn.calculate())
    }

    @Test
    fun `multiplies and sums`() {
        rpn.input(3)
        rpn.input(5)
        rpn.input(8)
        rpn.multiplication()
        rpn.input(7)
        rpn.addition()
        rpn.multiplication()
        assertEquals(141, rpn.calculate())
    }

    @Test
    fun `calculates the max number`() {
        rpn.input(3)
        rpn.input(8)
        rpn.input(5)
        rpn.max()
        assertEquals(8, rpn.calculate())
    }
}