import java.util.*
import kotlin.math.roundToInt
import kotlin.math.sqrt

class RpnCalculator {
    private val expressions = ArrayDeque<Expression>()

    fun calculate(): Int {
        if (expressions.isEmpty()) return 0
        if (expressions.size > 1) throw CalculationNotFinished()
        return result()
    }

    private fun result(): Int = retrieveOperand().reduce()

    fun input(number: Int) {
        expressions.push(Value(number))
    }

    fun addition() = addBinaryOperation { a, b -> a + b }
    fun subtraction() = addBinaryOperation { a, b -> a - b }
    fun multiplication() = addBinaryOperation { a, b -> a * b }
    fun division() = addBinaryOperation { a, b -> a / b }

    private fun addBinaryOperation(operation: (Int, Int) -> Int) {
        val secondOperand = retrieveOperand()
        val firstOperand = retrieveOperand()
        expressions.push(BinaryOperation(firstOperand, secondOperand, operation))
    }

    fun sqrt() {
        expressions.push(SquareRoot(retrieveOperand()))
    }

    private fun retrieveOperand(): Expression {
        if (expressions.isEmpty()) throw CalculationNotFinished()
        return expressions.poll()
    }

    fun max() {
        val operands = listOf<Expression>(*expressions.toTypedArray())
        expressions.clear()
        expressions += Max(operands)
    }

    class CalculationNotFinished : Exception()

    interface Expression {
        fun reduce(): Int
    }

    class BinaryOperation(
        private val firstOperand: Expression, private val secondOperand: Expression,
        private val operation: (Int, Int) -> Int
    ) : Expression {
        override fun reduce(): Int = operation(firstOperand.reduce(), secondOperand.reduce())
    }

    class Value(private val number: Int) : Expression {
        override fun reduce(): Int = number
    }

    class SquareRoot(private val operand: Expression) : Expression {
        override fun reduce(): Int = sqrt(operand.reduce().toDouble()).roundToInt()
    }

    class Max(private val operands: List<Expression>) : Expression {
        override fun reduce(): Int = operands.maxBy { it.reduce() }!!.reduce()
    }

}
